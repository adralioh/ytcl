# Changelog
## 1.1.0
### Changed
- Smooth out the download speed when downloading videos.

  This stops the speed from constantly changing when downloading fragmented
  files.

  You can disable this behavior by setting the `--speed-ema-weight` argument to
  *1*.

- The `$YTCL_SERVER` env-vars have been renamed to `$YTDL_SERVER`.

  `YTDL_SERVER` is less confusing since it matches the name of ytdl-server. This
  also allows other programs to use the same env-vars since the names are no
  longer tied to ytcl.

  Changed env-vars:
  | Old env-var          | New env-var          |
  | -------------------- | -------------------- |
  | YTCL_SERVER          | YTDL_SERVER          |
  | YTCL_SERVER_USERNAME | YTDL_SERVER_USERNAME |
  | YTCL_SERVER_PASSWORD | YTDL_SERVER_PASSWORD |

  The old env-vars are still recognized, but the new env-vars have priority if
  both are defined.

### Deprecated
- `$YTCL_SERVER` env-vars. See above for more information.

## 1.0.1
### Changed
- Don't print the download progress of finished files when
  `--output-format=progress`.

  This should make the log output easier to read when files aren't actively
  being downloaded.

- Don't print placeholder values when printing download progress.

  This makes the progress easier to read when downloading live chat.

  Before:

      210.7 KiB of ????.?? B at 4.88 KiB/s ETA --:-- (frag 8/??)

  After:

      210.7 KiB at 4.88 KiB/s (frag 8)
