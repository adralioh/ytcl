"""Base test classes for running unit tests"""

from __future__ import annotations

__all__ = ('TestParser',)

import unittest
import unittest.mock as mock
import uuid
from typing import TYPE_CHECKING

from ytcl import base, get, http

if TYPE_CHECKING:
    from typing import Any


class TestParser(unittest.TestCase):
    __slots__ = ('mock_opener', 'get_parser')

    mock_opener: mock.Mock
    """Mocked http.opener object that's passed to the parser"""
    get_parser: get.Get
    """`Get` subparser

    You must call `setUpGet()` in each test before using this.
    """

    _DEFAULT_BASE_KWARGS = {
        'detach': True,
        'interval': 1.0,
        'output_format': base.FormatEnum.PROGRESS,
        'use_escape_codes': False,
        'output_file': None,
        'progress_mode': base.ProgressEnum.NEW_LINE,
        'speed_ema_weight': 0.2
    }
    """Default kwargs that are passed to all parsers when initialized

    They can be overridden by passing kwargs to the setup functions.
    """
    _DEFAULT_GET_KWARGS = {
        # Random UUID
        'job_id': uuid.UUID('d36b1ded-1d58-4db3-86ab-1c596d3e9b64'),
        'log_lines': 0
    }
    """Additonal kwargs that are passed to `Get` parsers"""

    def setUpGet(self, **init_kwargs: Any) -> None:
        """Set up a `Get` parser for testing

        The parser will be assigned to the `get_parser` attribute.

        Any kwargs that are given will be passed to the initializer.
        """
        self.mock_opener = mock.Mock(http.Opener)
        self.addCleanup(delattr, self, 'mock_opener')

        kwargs: dict[str, Any] = self._DEFAULT_BASE_KWARGS.copy()
        kwargs.update(self._DEFAULT_GET_KWARGS)

        kwargs['opener'] = self.mock_opener
        kwargs.update(init_kwargs)

        self.get_parser = get.Get(**kwargs)
        self.addCleanup(delattr, self, 'get_parser')
