"""Unit tests for the `http` module"""

from __future__ import annotations

__all__ = ('TestOpenerFromEnv',)

import logging
import os
import unittest
import unittest.mock as mock
from typing import TYPE_CHECKING

from ytcl import http
from ytcl.error import MissingYTDLServerError

if TYPE_CHECKING:
    from typing import Mapping, Optional


class TestOpenerFromEnv(unittest.TestCase):
    __slots__ = ()

    def _test_from_env(
        self, env_vars: Mapping[str, str], given_server: Optional[str],
        given_username: Optional[str], given_password: Optional[str],
        expected_server: str, expected_username: Optional[str],
        expected_password: Optional[str]
    ) -> None:
        """Helper function for testing the `Opener.from_env()` method

        `env_vars` is a mapping of env-vars that will be accessible by
        the method.

        `given_server`, `given_username`, and `given_password` will be
        passed directly to the method. These are used when the user
        supplies a value via command-line.

        `expected_server`, `expected_username`, and `expected_password`
        are the values that the opener is expected to be initialized
        with. These will be sourced either from the given args, or via
        env-var.
        """
        # Mock `os.environ` so that we can set our own env-vars.
        with mock.patch.dict(os.environ, env_vars, clear=True):
            opener = http.Opener.from_env(
                root_url=given_server, user=given_username,
                password=given_password
            )

        # This is run outside of a subtest because auth doesn't work if
        # the URI doesn't match.
        self.assertEqual(
            expected_server, opener.root_url,
            msg='ytdl-server root URL is different from expected.'
        )

        # Get the username and password from the opener's password
        # manager.
        actual_username, actual_password = (
            opener._password_mgr.find_user_password(
                realm=None, authuri=expected_server
            )
        )

        with self.subTest(msg='Check ytdl-server username'):
            self.assertEqual(expected_username, actual_username)
        with self.subTest(msg='Check ytdl-server password'):
            self.assertEqual(expected_password, actual_password)

    def test_all_given(self) -> None:
        """All args are given explicitly"""
        self._test_from_env(
            env_vars={},
            given_server='https://ytdl.example.com',
            given_username='USER', given_password='PASS',
            expected_server='https://ytdl.example.com',
            expected_username='USER', expected_password='PASS'
        )

    def test_all_env(self) -> None:
        """All args are given via env-var"""
        self._test_from_env(
            env_vars={
                'YTDL_SERVER': 'https://ytdl.example.com',
                'YTDL_SERVER_USERNAME': 'USER',
                'YTDL_SERVER_PASSWORD': 'PASS'
            },
            given_server=None,
            given_username=None, given_password=None,
            expected_server='https://ytdl.example.com',
            expected_username='USER', expected_password='PASS'
        )

    def test_given_override_env(self) -> None:
        """Args that are given explicitly have precedence over env_vars"""
        self._test_from_env(
            env_vars={
                'YTDL_SERVER': 'https://env.example.com',
                'YTDL_SERVER_USERNAME': 'ENV_USER',
                'YTDL_SERVER_PASSWORD': 'ENV_PASS'
            },
            given_server='https://given.example.com',
            given_username='GIVEN_USER', given_password='GIVEN_PASS',
            expected_server='https://given.example.com',
            expected_username='GIVEN_USER', expected_password='GIVEN_PASS'
        )

    def test_deprecated_env(self) -> None:
        """Deprecated env-var aliases

        A deprecation warning should be logged.
        """
        with self.assertLogs(level=logging.WARNING):
            self._test_from_env(
                env_vars={
                    'YTCL_SERVER': 'https://ytdl.example.com',
                    'YTCL_SERVER_USERNAME': 'USER',
                    'YTCL_SERVER_PASSWORD': 'PASS'
                },
                given_server=None,
                given_username=None, given_password=None,
                expected_server='https://ytdl.example.com',
                expected_username='USER', expected_password='PASS'
            )

    def test_deprecated_precedence(self) -> None:
        """Current env-vars have precedence over deprecated env-vars"""
        self._test_from_env(
            env_vars={
                'YTDL_SERVER': 'https://current.example.com',
                'YTDL_SERVER_USERNAME': 'CURRENT_USER',
                'YTDL_SERVER_PASSWORD': 'CURRENT_PASS',

                'YTCL_SERVER': 'https://old.example.com',
                'YTCL_SERVER_USERNAME': 'OLD_USER',
                'YTCL_SERVER_PASSWORD': 'OLD_PASS'
            },
            given_server=None,
            given_username=None, given_password=None,
            expected_server='https://current.example.com',
            expected_username='CURRENT_USER', expected_password='CURRENT_PASS'
        )

    @mock.patch('ytcl.http.prompt_pass', autospec=True)
    def test_password_prompt_given(self, mock_prompt: mock.MagicMock) -> None:
        """Prompt for password if only the username is given"""
        # Mock the password that will be returned by `prompt_pass()`
        mock_prompt.return_value = 'PASS'

        self._test_from_env(
            env_vars={},
            given_server='https://ytdl.example.com',
            given_username='USER', given_password=None,
            expected_server='https://ytdl.example.com',
            expected_username='USER', expected_password='PASS'
        )

    @mock.patch('ytcl.http.prompt_pass', autospec=True)
    def test_password_prompt_env(self, mock_prompt: mock.MagicMock) -> None:
        """Prompt for password if only the username env-var is defined"""
        # Mock the password that will be returned by `prompt_pass()`
        mock_prompt.return_value = 'PASS'

        self._test_from_env(
            env_vars={
                'YTDL_SERVER_USERNAME': 'USER'
            },
            given_server='https://ytdl.example.com',
            given_username=None, given_password=None,
            expected_server='https://ytdl.example.com',
            expected_username='USER', expected_password='PASS'
        )

    def test_server_not_given(self) -> None:
        """Exception is raised when the server URL isn't given"""
        with self.assertRaises(MissingYTDLServerError):
            self._test_from_env(
                env_vars={},
                given_server=None,
                given_username=None, given_password=None,
                expected_server='N/A',
                expected_username=None, expected_password=None
            )
