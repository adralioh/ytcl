"""Unit tests for `Base.base.format_progress()`"""

from __future__ import annotations

__all__ = ('TestFormatProgress', 'TestSpeedEMA')

from typing import TYPE_CHECKING

from .base import TestParser

if TYPE_CHECKING:
    from collections.abc import Mapping
    from typing import Any


class TestFormatProgress(TestParser):
    __slots__ = ()

    def _test_format_progress(
        self, progress: Mapping[str, Any], expected: str, real_time: bool
    ) -> None:
        self.setUpGet()
        formatted = self.get_parser.format_progress(progress, real_time)
        self.assertEqual(expected, formatted)

    def test_downloading_realtime(self) -> None:
        """Downloading a video in real-time"""
        self._test_format_progress(
            real_time=True,
            progress={
                'status': 'downloading',
                'filename': 'FILENAME.mp4',
                'tmpfilename': 'FILENAME.mp4.part',
                'downloaded_bytes': 399052633,
                'total_bytes': 1705809435,
                'elapsed': 44.98340940475464,
                'eta': 1216,
                'speed': 1074906.3058313173
            },
            expected=' 23.4% of 1.59 GiB at    1.03 MiB/s ETA    20:16'
        )

    def test_downloading(self) -> None:
        """Downloading a video"""
        self._test_format_progress(
            real_time=False,
            progress={
                'status': 'downloading',
                'filename': 'FILENAME.mp4',
                'tmpfilename': 'FILENAME.mp4.part',
                'downloaded_bytes': 399052633,
                'total_bytes': 1705809435,
                'elapsed': 44.98340940475464,
                'eta': 1216,
                'speed': 1074906.3058313173
            },
            expected=(
                'Downloading [23.4% of 1.59 GiB at 1.03 MiB/s ETA 20:16] '
                "'FILENAME.mp4'"
            )
        )

    def test_finished(self) -> None:
        """Download finished"""
        self._test_format_progress(
            real_time=False,
            progress={
                'status': 'finished',
                'filename': 'FILENAME.webm',
                'downloaded_bytes': 37777147,
                'total_bytes': 37777147,
                'elapsed': 4.36859130859375
            },
            expected="Finished [36.03 MiB in 00:04] 'FILENAME.webm'"
        )

    def test_finished_realtime(self) -> None:
        """Download finished in real-time

        Real-time progress is only printed when the status is
        'downloading', so this should return an empty string.
        """
        self._test_format_progress(
            real_time=True,
            progress={
                'status': 'finished',
                'filename': 'FILENAME.webm',
                'downloaded_bytes': 37777147,
                'total_bytes': 37777147,
                'elapsed': 4.36859130859375
            },
            expected=''
        )

    def test_fragmented_realtime(self) -> None:
        """Downloading a fragmented video in real-time"""
        self._test_format_progress(
            real_time=True,
            progress={
                'status': 'downloading',
                'filename': 'FILENAME.mp4',
                'tmpfilename': 'FILENAME.mp4.part',
                'downloaded_bytes': 557388,
                'total_bytes_estimate': 6037305.875,
                'elapsed': 3.197659969329834,
                'eta': 31,
                'speed': 613985.8262623695,
                'fragment_index': 7,
                'fragment_count': 83
            },
            expected=(
                '  9.2% of    ~5.76 MiB at   599.6 KiB/s ETA '
                '   00:31 (frag  7/83)'
            )
        )

    def test_fragmented(self) -> None:
        """Downloading a fragmented video"""
        self._test_format_progress(
            real_time=False,
            progress={
                'status': 'downloading',
                'filename': 'FILENAME.mp4',
                'tmpfilename': 'FILENAME.mp4.part',
                'downloaded_bytes': 557388,
                'total_bytes_estimate': 6037305.875,
                'elapsed': 3.197659969329834,
                'eta': 31,
                'speed': 613985.8262623695,
                'fragment_index': 7,
                'fragment_count': 83
            },
            expected=(
                'Downloading '
                '[9.2% of ~5.76 MiB at 599.6 KiB/s ETA 00:31 (frag 7/83)] '
                "'FILENAME.mp4'"
            )
        )

    def test_live_chat_realtime(self) -> None:
        """Downloading live chat in real-time"""
        self._test_format_progress(
            real_time=True,
            progress={
                'status': 'downloading',
                'filename': 'FILENAME.live_chat.json',
                'tmpfilename': 'FILENAME.live_chat.json.part',
                'downloaded_bytes': 10048314,
                'elapsed': 17.136795043945312,
                'speed': 268024.1357428314,
                'fragment_index': 32,
                'fragment_count': None
            },
            expected='   9.58 MiB at  261.74 KiB/s (frag 32)'
        )

    def test_live_chat(self) -> None:
        """Downloading live chat"""
        self._test_format_progress(
            real_time=False,
            progress={
                'status': 'downloading',
                'filename': 'FILENAME.live_chat.json',
                'tmpfilename': 'FILENAME.live_chat.json.part',
                'downloaded_bytes': 10048314,
                'elapsed': 17.136795043945312,
                'speed': 268024.1357428314,
                'fragment_index': 32,
                'fragment_count': None
            },
            expected=(
                'Downloading [9.58 MiB at 261.74 KiB/s (frag 32)] '
                "'FILENAME.live_chat.json'"
            )
        )


class TestSpeedEMA(TestParser):
    __slots__ = ()

    def _check_progress(
        self, current_speed: float, filename: str, expected_ema: str
    ) -> None:
        progress = {
            'status': 'downloading',
            'speed': current_speed,
            'filename': filename,

            # Unimportant fields
            'downloaded_bytes': 1024,
            'total_bytes': 10240,
            'elapsed': 5,
            'eta': 50
        }

        formatted = self.get_parser.format_progress(progress, True)
        self.assertIn(expected_ema, formatted)

    def test_ema(self) -> None:
        """Download speed EMA"""
        self.setUpGet(speed_ema_weight=0.2)

        self._check_progress(
            current_speed=200.0, filename='FILE1', expected_ema='200.0 B/s'
        )
        self._check_progress(
            current_speed=250.0, filename='FILE1', expected_ema='210.0 B/s'
        )
        self._check_progress(
            current_speed=400.0, filename='FILE1', expected_ema='248.0 B/s'
        )

        with self.subTest(msg='Reset EMA when filename changes'):
            self._check_progress(
                current_speed=600.0, filename='FILE2', expected_ema='600.0 B/s'
            )
            self._check_progress(
                current_speed=400.0, filename='FILE2', expected_ema='560.0 B/s'
            )
            self._check_progress(
                current_speed=80.0, filename='FILE3', expected_ema='80.0 B/s'
            )

    def test_ema_disabled(self) -> None:
        """Download speed EMA is disabled

        Setting the weight to 1 means that the current speed will be
        displayed as-is.
        """
        self.setUpGet(speed_ema_weight=1)

        self._check_progress(
            current_speed=100.0, filename='FILE1', expected_ema='100.0 B/s'
        )
        self._check_progress(
            current_speed=900.0, filename='FILE1', expected_ema='900.0 B/s'
        )
